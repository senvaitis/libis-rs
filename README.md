# libis-rs
This repo is meant to be private and forbidden from usage without written permission from Kazimieras Senvaitis and Martynas Mažvydas National Library of Lithuania (LNB).

This project is responsible for research & development of libis recommendation system.

## Instruction
1. Install Python (developed with 3.9)
2. Install Jupyter
3. Install dependencies according to requirements.txt
4. Add data to /data/libis
5. Build model (train and evaluate)
6. Perform practical testing

## Algorithm is based on the following articles:
https://www.kaggle.com/jamesloy/deep-learning-based-recommender-systems/notebook
https://towardsdatascience.com/deep-learning-based-recommender-systems-3d120201db7e
