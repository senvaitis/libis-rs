import pandas as pd

from recommenders.utils import ncf_utils, tf_idf_utils, kmeans_utils


def generate_recommendations(matching_user_df, users, orders, orders_with_negatives, user_interacted_items,
                             records, all_record_ids, model_ncf, model_kmeans, tfidf_cosine_sim_df):
    # matching_user_df = users[users['libis_id'] == libis_user_id]
    # if matching_user_df.empty:
    #     pd.DataFrame()

    user = matching_user_df.to_records()[0]
    order_history = orders.loc[orders['user_id'] == user.id]

    if len(order_history) > 10:
        cf_recommendations_count = 10
        df_recommendations_count = 2
        cbf_recommendations_count = 3
    elif (order_history) > 3:
        cf_recommendations_count = 0
        df_recommendations_count = 10
        cbf_recommendations_count = 5
    else:
        cf_recommendations_count = 0
        df_recommendations_count = 15
        cbf_recommendations_count = 0

    recommendations_cf = ncf_utils.generate_recommendations(matching_user_df, user_interacted_items, records,
                                                            all_record_ids, model_ncf).head(cf_recommendations_count)
    recommendations_df = kmeans_utils.generate_recommendations(matching_user_df, users, model_kmeans,
                                                               orders_with_negatives, user_interacted_items,
                                                               records).head(df_recommendations_count)
    recommendations_cbf = tf_idf_utils.generate_recommendations(matching_user_df, orders, tfidf_cosine_sim_df, records,
                                                                user_interacted_items).head(
        cbf_recommendations_count)
    aggregated_recommendations = pd.concat([recommendations_cf, recommendations_df, recommendations_cbf])
    aggregated_recommendations = aggregated_recommendations.drop_duplicates(subset='id', keep="last")
    return aggregated_recommendations