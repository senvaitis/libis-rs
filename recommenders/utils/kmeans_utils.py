import pandas as pd
from tqdm.notebook import tqdm

def generate_recommendations(user_df, users, model_kmeans, orders_with_negatives, user_interacted_items, records):
    user = user_df.to_records()[0]

    user_df = pd.DataFrame({
        "id":[users['id'].max()+2],
        "age_group":[user.age_group],
        "education":[user.education],
        "gender":[user.gender],
    })
    normalize_users(users_df=user_df)

    predicted_label = model_kmeans.predict(user_df.drop(columns ='id'))

    cluster_users = []

    cluster_orders = orders_with_negatives.drop(columns='timestamp')
    for i in tqdm(range(len(model_kmeans.labels_))):
        if model_kmeans.labels_[i] in predicted_label:
            cluster_users.append(float(i+1))

    cluster_orders = cluster_orders[cluster_orders['user_id'].isin(cluster_users)]
    cluster_orders = cluster_orders.groupby('record_id').filter(lambda x : len(x) > 10)
    interacted_items = user_interacted_items.get(user.id)
    if interacted_items is not None:
        cluster_orders = cluster_orders[~cluster_orders['record_id'].isin(interacted_items)]
    new_orders = cluster_orders.groupby('record_id', as_index=False)['order_count'].mean()
    recommend = new_orders.sort_values(by=['order_count'], ascending=False).head(10)

    recommend = pd.merge(recommend, records, left_on='record_id', right_on='id', how='left')

    result = records.loc[records['id'].isin(recommend['record_id'].tolist())]
    result['recommender'] = 'DF'
    return result

def normalize_users(users_df):
    users_df.loc[~users_df["gender"].isin(['FEMALE', 'MALE']), "gender"] = "Unknown"
    users_df.loc[~users_df["education"].isin(['f', 'e', 'd', 'c', 'b', 'a9', 'a', 'a1', 'a2', 'a3']), "education"] = "Unknown"
    users_df.loc[~users_df["age_group"].isin(
        ['0-2', '3-5', '6-8', '9-11', '12-14', '15-17', '18-29', '30-39', '40-49', '50-59', '60-69', '70-79', '80-89',
         '90+']), "age_group"] = "Unknown"
    # users.loc[~users["is_currently_studying"].isin(['f', 't']), "is_currently_studying"] = 'f'

    users_df["gender"].fillna("Unknown", inplace=True)
    users_df["education"].fillna("Unknown", inplace=True)
    users_df["age_group"].fillna("Unknown", inplace=True)
    # users["is_currently_studying"].fillna("Unknown", inplace = True)

    users_df['gender'].replace(['FEMALE', 'MALE', 'Unknown'], [0, 1, 1], inplace=True)

    users_df['education'] = users_df['education'].replace(['f'], [0]) # Ikimokyklinis
    users_df['education'].replace(['e'], [0.1], inplace=True) # Pradinis
    users_df['education'].replace(['d'], [0.5], inplace=True) # Pagrindinis
    users_df['education'].replace(['c'], [0.7], inplace=True) # Vidurinis
    users_df['education'].replace(['b'], [0.75], inplace=True) # Aukštesnysis (profesinis)
    users_df['education'].replace(['a9'], [0.75], inplace=True) # Aukštasis-neuniversitetinis
    users_df['education'].replace(['a'], [0.9], inplace=True) # Aukštasis
    users_df['education'].replace(['a1'], [0.9], inplace=True) # Bakalauras
    users_df['education'].replace(['a2'], [0.95], inplace=True) # Magistras
    users_df['education'].replace(['a3'], [1], inplace=True) # Doktorantas
    users_df['education'].replace(['Unknown'], [0.7], inplace=True)

    users_df['age_group'] = users_df['age_group'].replace(['0-2'], [0])
    users_df['age_group'].replace(['3-5'], [0.1], inplace=True)
    users_df['age_group'].replace(['6-8'], [0.2], inplace=True)
    users_df['age_group'].replace(['9-11'], [0.4], inplace=True)
    users_df['age_group'].replace(['12-14'], [0.6], inplace=True)
    users_df['age_group'].replace(['15-17'], [0.8], inplace=True)
    users_df['age_group'].replace(['18-29'], [1], inplace=True)
    users_df['age_group'].replace(['30-39'], [1], inplace=True)
    users_df['age_group'].replace(['40-49'], [1], inplace=True)
    users_df['age_group'].replace(['50-59'], [1], inplace=True)
    users_df['age_group'].replace(['60-69'], [1], inplace=True)
    users_df['age_group'].replace(['70-79'], [1], inplace=True)
    users_df['age_group'].replace(['80-89'], [1], inplace=True)
    users_df['age_group'].replace(['90+'], [1], inplace=True)
    users_df['age_group'].replace(['Unknown'], [1], inplace=True)