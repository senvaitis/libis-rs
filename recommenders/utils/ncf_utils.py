import numpy as np
import torch

def generate_recommendations(user_df, user_interacted_items, records, all_record_ids, model_ncf):
    user = user_df.to_records()[0]

    interacted_items = user_interacted_items[user.id]
    not_interacted_items = set(all_record_ids) - set(interacted_items)

    selected_not_interacted = list(np.random.choice(list(not_interacted_items), 10000))
    predicted_labels = np.squeeze(model_ncf(torch.tensor([user.id] * 10000),
                                            torch.tensor(selected_not_interacted)).detach().numpy())

    top10_items = [selected_not_interacted[i] for i in np.argsort(predicted_labels)[::-1][0:10].tolist()]
    recommend = records.loc[records['id'].isin(top10_items)]
    recommend['recommender'] = 'CF'
    return recommend