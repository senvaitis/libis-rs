import pandas as pd
from datetime import datetime
from dateutil.relativedelta import relativedelta

def generate_recommendations(user_df, orders, tfidf_cosine_sim_df, records, user_interacted_items):
    user = user_df.to_records()[0]

    user_last_order_history = orders[
        (orders['user_id'] == user.id)
        & (orders['timestamp'] > (datetime.now() - relativedelta(months=6)))
    ].sort_values(['timestamp'], ascending = [False]).head(20)
    user_last_order_history = pd.merge(user_last_order_history, records, left_on='record_id', right_on='id', how='left')
    user_last_order_history = user_last_order_history.drop_duplicates(subset='record_id', keep="last")
    recommendations = pd.DataFrame()

    for index, row in user_last_order_history.iterrows():
        recommendations_by_ordered_record = genre_recommendations(row['code'], tfidf_cosine_sim_df, records, 5)
        recommendations = pd.concat([recommendations, recommendations_by_ordered_record])
        recommendations = recommendations.drop_duplicates(subset='id', keep="last")


    interacted_items = user_interacted_items[user.id]
    recommendations = recommendations[~recommendations['id'].isin(interacted_items)].sample(10, replace=True)
    recommendations = recommendations.drop_duplicates(subset='id', keep="last")
    recommendations['recommender'] = 'CBF'
    return recommendations

def genre_recommendations(i, M, items, k=10):
    """
    Recommends records based on a similarity dataframe

    Parameters
    ----------
    i : str
        Movie (index of the similarity dataframe)
    M : pd.DataFrame
        Similarity dataframe, symmetric, with records as indices and columns
    items : pd.DataFrame
        Contains both the title and some other features used to define similarity
    k : int
        Amount of recommendations to return

    """
    ix = M.loc[:,i].to_numpy().argpartition(range(-1,-k,-1))
    closest = M.columns[ix[-1:-(k+2):-1]]
    # if closest.shape[1] == 1:
    # closest = closest.drop(i, errors='ignore')
    # return pd.DataFrame(closest).merge(items, on=['title']).head(k)


    closest_df = pd.DataFrame(closest)
    closest_df = closest_df.drop_duplicates(subset='code', keep="last")
    closest_df = closest_df[closest_df['code'] != i]
    # closest = closest.drop(i, errors='ignore')
    return closest_df.merge(items, on=['code']).head(k)
    # closest = closest.drop(i, errors='ignore')
    # return pd.DataFrame(closest).merge(items, on=['title']).head(k)
    # else:
    #     return pd.DataFrame()
